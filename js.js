document.getElementById('phoneNumber').onkeypress = function(e) {
  e = e || event;
  if (e.ctrlKey || e.altKey || e.metaKey) return;
  var chr = getChar(e);
  if (chr == null) return;
  if (chr < '0' || chr > '9') {
    return false;
  }
}
function getChar(event) {
  if (event.which == null) {
    if (event.keyCode < 32) return null;
    return String.fromCharCode(event.keyCode)
  }
  if (event.which != 0 && event.charCode != 0) {
    if (event.which < 32) return null;
    return String.fromCharCode(event.which)
  }
  return null;
}

var timeElem = document.getElementById('progress'),
	timeCircle = document.getElementById('timer'),
	timeIcon = document.getElementById('lirax-icon'),
	timeInformer = document.getElementById('lira-modal-informer'),
	timeField = document.getElementById('lirax-modal-field'),
	timeExample = document.getElementById('lirax-modal-example'),
	timeTitle = document.getElementById('lirax-modal-title'),
	t = 0, z = 0, s = 30, ms = 9, d = '', countDownTime;

function startTime() {
	if (s != 0 || ms !=0) {
		if(ms == 0) {
			ms = 9; 
			s--;
			y = t;
			z = z + 100/30;
			t = Math.round(z);
			timeCircle.classList.add('p' + t);
			timeCircle.classList.remove('p' + y);
		} else {
			ms--;
		}
	} else {
		clearInterval(countDownTime);
	}
	if (ms & 1) {
		d.length < 3 ? d = d + '.' : d = '';
		document.getElementById('anidot').innerHTML = d;
	}
	document.getElementById('progress').innerHTML = s + '.' + ms;
}
document.getElementById('startTimer').onclick = function() {
	countDownTime = setInterval(startTime, 100);
	timeCircle.classList.remove('lirax-hidden');
	timeInformer.classList.remove('lirax-hidden');
	timeIcon.classList.add('lirax-hidden');
	timeField.classList.add('lirax-hidden');
	timeExample.classList.add('lirax-hidden');
	timeTitle.innerHTML = 'Заказ обратно звонка принят! <br> Наш менеджер уже звонит Вам';
	responseTime = new Date(Date.now() + (1000*30));
	requestAnimationFrame(startTime);
}